¡Muy fácil!
#############

:slug: muy-fàcil
:date: 2023-01-29 00:06
:tag: difícil, aprender, español, idiomas
:category: Sujeto
:corrector: Fernanhaiku, cristinaaquafina101 
:prompt:  ¿Qué es lo más difícil de tu idioma que estás aprendiendo y cómo lo afrontas?
:prompt_author: Lii

¡Que fácil! En el momento que he comenzado este texto, tenía un montón de cosas
que decir sobre la facilidad del español. De hecho, escribí muchas ideas pero
habían demasiadas, así comienzo de nuevo.

Cuando descubrí este idioma, hace un poco menos de uno año, fue una revolución.
En mi miente, la policía trató de mantener el orden, el estado normal de mis
pensamientos, el francés. Pero los revolucionarios hablando español rehusaron el
silencio. Cantaron "¡Que fácil!"

De verdad, me pareció tan fácil. Español y francés tienen muchas palabras con
las mismas raíces. ¡Que fácil! La pronunciación, los acentos escritos y
hablados, las letras, la gramática, la conjugación ... ¡Todo es muy fácil!

"Muy" ... "Moui moui moui" esta facilidad es mi mayor problema. "muy" suena como
la palabra francesa "moui" Esta significa que dice "si" pero piensa que "no".
Por eso, cada vez que leo "muy", las ruinas de mi mundo interno piensan "un
poco". En medio de estos combates, se puede oír que "entender" no es "entendre"
(más o menos : "escuchar", "oír"), "palabra" es sólo una palabra, no es una
conversación, etc.

¡No es tan fácil! Cuando escribo, no sé si las frases que pienso son malas
traducciones o buenas. Me pierdo entre los idiomas. Pero creo que, poco a poco,
voy a mejorar. Trato de pensar, escribir o hablar sólo con
palabras, expresiones y frases que conozco. Necesito leer, aprender
mucho y recordar mucho más. No es fácil pero es agradable. Lo más
difícil es aprender el vocabulario fácil. Si veo una palabra casi
igual que en francés, necesito más esfuerzos que en una palabra
poco común. Sí, lo siento pero hay un poco de palabras extrañas (me
gustan).

Finalmente : ¿español? ¡no es tan fácil! Pero me gusta tanto. Espero que un día
de estos os regalaré textos hermosos. Por ahora, estos son como son. Oigo dentro
de mi mente : "Las palabras unidas jamás serán vencidas".
