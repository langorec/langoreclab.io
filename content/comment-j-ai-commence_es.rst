Como he empezado a aprender español
###################################

:date: 2023-02-02 09:00
:tag: apprendre, aprender
:category: Apprendre
:lang: es
:slug: comment-j-ai-commence-a-apprendre-l-espagnol
:status: draft
:corrector: cristinaaquafina101, cthylla
:langcorrect: https://langcorrect.com/journal/como-he-empezado-a-aprender-espa%C3%B1ol-1a-parte/

Aquí voy a tratar de explicar como aprendí español. Comencé hace un año, desde
la primavera en 2022. Pero creo que debo volver un poco más en el pasado y voy a
hablar del español después.

Antes la enseñanza del español
------------------------------

Mi primer idioma es el francés. En la escuela, aprendí inglés y también alemán.
No tengo muchas cosas que recordar de este periodo. Si aprendí a leer en inglés
porque buscando la solución a un problema con el computador, no tenía otra forma
que buscarla en este idioma. Así es como si estoy viviendo en un país donde todo
el mundo habla inglés. Pero es solo un país virtual. Si acabo de leer, es mas
difícil de escribir y hablar...

Olvidé todo acerca del alemán pero, un día, hay alguien me habló de Duolingo. Me
gustó que fue al igual que un juego y lo comencé aprender otra vez. Me gustó
mucho y fue un nuevo hábito de todos los días. Pero Duolingo tiene problemas y
al final, no me gustó tanto. No quise tener que seguir viendo siempre lo mismo,
siempre las faltas de traducción que en verdad no importa. Abandoné Duolingo.

Pero me pareció triste que no continué. Un día, una amiga me avisó sobre un
libro para aprender idiomas. Lo leí. Se llama `Fluent forever
<https://annas-archive.org/search?q=fluent+forever>`_, este libro debe decir lo
mismo que otros libros pero en este momento fue algo muy nuevo. Fue mas que
aprender solo un idioma, pero no es el punto de este texto. Creo que la meta del
libro es venderte productos de un compañía con el mismo nombre. No me interesa.

Este libro fue el principio de mas descubrimientos. En este libro, oí de un
programa llamando `Anki <https://apps.ankiweb.net>`_. Tal vez hablaría de eso
mas tarde, es un sistema de "flashcards", algo con preguntas y respuestas. Las
primeras veces fueron muy difíciles, como todo el mundo, pero me he convertido.
Hacer las flashcards, volver a verlas, tratar de leer en mi memoria como leo en
un gran libro ... Fue fascinante y mucho mas un juego que Duolinguo.

Aprendí mucho vocabulario y un día me perdí en todas las palabras y abandoné
todo. ¡Scheiße! Un par de meses después, empiezo con el italiano pero no logré
de nuevo. Merda! Y de un día a otro, ¿quién estaba a la puerta de mí cabeza?
¡Hola Español!

Para empezar
------------

Con el alemán y el italiano pude tratar muchas cosas. Comencé a aprender el
alfabeto fonético. Así, iba a hacer algo parecido a lo de Fluent Forever:

* buscar libros para la gramática
* estudiar precisamente la pronunciación, la escritura y usar flashcards para
  aprender las reglas con ejemplos y distinguir los pares mínimos.
* crear una lista de las palabras más frecuentes y convertirlas en flashcards
* estudiar la conjugación para aprender todas las formas conjugadas de los
  verbos regulares (hablar, deber, vivir…) y los irregulares (ser, sentir,
  pensar…)
* al mismo tiempo, buscar otra forma de contenido por descubrir el universo:
  de la geografía, canciones, etc.
* leer sobre la gramática y creer cartas por cada regla, siempre con ejemplos
* añadir frases con el vocabulario para que las palabras estén su contexto
* encontrar algo fácil de leer para empezar
* escribir textos con las palabras y las frases que conozco (como aquí mismo)
* hablar con alguien

Fue el plan, pero nada pasó como lo había imaginado. Esscribiendo estas ideas,
me doy cuenta de que necesitarán más explicaciones. Creo que sería interesante,
pero necesito escribir otros textos para ello. Ahora, estoy estudiendo la
conjugación y la gramática. Aprendo algunas frases y escribo algunos textos. No
hablo nada por el momento.

Les grandes idées que j'essaye de suivre
----------------------------------------

Avant de rentrer dans le détail voici quelques idées que j'essaye d'appliquer :

Faut s'marrer
^^^^^^^^^^^^^

J'ai besoin que les choses soient ludiques pour pratiquer quotidiennement. De
fait ça l'est et ça l'est tellement que ne pas réviser me fout mal. Il se trouve
qu'après plusieurs mois d'apprentissage j'ai merdé avec l'application AnkiDroid
que j'utilise pour passer en revue mes cartes ce qui faisait que je ne pouvais
passer en revue mes cartes sans me coller derrière un ordi. Le temps que je
résolve le problème, j'ai passé quelques jours assez pénibles.

Travailler l'oral au maximum
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

J'essaye d'avoir la bonne prononciation en tête, d'utiliser des sons prononcés
par des locuteurs et si possible assez variés. Ça force à rentrer dans la
variété de la langue. D'autant qu'avec l'espagnol, les possibilités sont assez
grandes à travers les continents où il est parlé.

Ne pas chercher à traduire
^^^^^^^^^^^^^^^^^^^^^^^^^^

Bien sûr, intérieurement je passe beaucoup de temps à traduire du français à
l'espagnol et inversement. Mais j'essaye d'éviter ça au maximum en apprenant de
préférence avec des images, des définitions en espagnol lorsque c'est plus
conceptuel voire une traduction en anglais pour les phrases. C'est imparfait
mais mieux que rien, et ça rend les choses plus ludiques.

À présent
---------

J'ai évoqué plus haut l'état actuel de mon apprentissage. Je ne prétends pas que
a méthode est bonne, efficace ou quoi que ce soit. La seule qualité que je lui
trouve est qu'elle me motive à continuer chaque jour. Je la détaille ici pour
en garder une trace, peut-être qu'un jour viendra où je voudrais l'évaluer, me
rappeler par où je suis passé ? Je vais donc continuer à écrire sur ce sujet.
Certains articles reviendront sur ce qui s'est passé avant cette fin janvier
2023 où je frappe ces mots. D'autres s'intéresseront à l'apprentissage qui est
en cours.


Petit état des lieux jusqu'ici
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour finir, je vais simplement essayer de noter la temporalité avec laquelle les
choses se sont déroulées d'après les dates de création de mes cartes. Ça ne
présente peut-être que peu d'intérêt pour le lecteur mais faut que je me note ça
quelque part et quelque part c'est ici :

* **début avril 2022** : je sais que j'ai ajouté quelques cartes de géographie donc
  j'avais forcément révisé les bases de la prononciation.
* **en mai** :

  * début mai, le vocabulaire le plus fréquent est ajouté (mais pas encore étudié)
  * **mi-mai, je commence enfin l'étude du vocabulaire** et je rajoute quelques
    règles de prononciation telles que: « Comment prononce-t-on ⟨v⟩ comme dans
    vivir ? » / « Pareil qu'un ⟨b⟩ comme dans /biˈβiɾ/ »
  * **fin mai, j'attaque la conjugaison** : le nom des temps conjugués, principes de
    construction des terminaisons, les verbes modèles : hablar, deber et vivir.
    À partir de là, le rythme est d'environ 3 temps par verbes et une dizaine de
    mots de vocabulaire par jour. Voir moins.

* **fin juillet** : le rythme des conjugaisons s'intensifie, j'apprends un verbe
  par jour. J'y reviendrais surement en expliquant comme j'ai appris les verbes
  mais ça a duré 2 semaines maximum.

* **mi-aout : je commence à ajouter des phrases** à mon carquois, environ 5 par jour
* **début septembre : j'en ai fini avec la conjugaison**
* **tout le mois de septembre je n'apprends quasiment aucun nouveau mot de
  vocabulaire**
* **début octobre** : le rythme redevient normal
* **mi-octobre** : j'ajoute quelques règles relatives à la conjugaison. Par
  exemple : "Généralement, on accentue les verbes conjugués sur la première
  voyelle de la terminaison." C'est aussi le début de **l'ajout en masse de
  phrases** avec seulement une traduction anglaise, parfois mauvaise. Le rythme
  est d'environ 20 phrases par jour (ex: "Soy yo").
* **mi-novembre** : je n'ajoute plus de vocabulaire, pour diverses raisons
* **mi-décembre** : j'ajoute du vocabulaire dans le but de **lire mon premier livre**
* **janvier 2023** : **j'écris mes premiers textes**, je rajoute du vocabulaire pour
  lire un autre livre et je crée de nouvelles cartes, notamment des paires de
  mots tels que "el/él" pour bien les distinguer. J'ajoute aussi des règles sur
  l'accentuation.

On voit que je n'applique pas ma méthode puisque au moins 6 mois après avoir
commencé, j'en suis encore aux règles de prononciation. Je développerais
peut-être mais il y a une sorte de va-et-vient nécessaire pour comprendre ce
qu'il faut apprendre. J'ai compris l'intérêt de distinguer el et él après de
nombreuses corrections reçues en écrivant en espagnol.

À suivre donc.
