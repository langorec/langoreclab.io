Empezar un libro, no lo terminar
################################

:date: 2023-02-05 19:00
:tag: leer 
:category: meta
:corrector: gideos
:prompt: ¿Cuál fue el último libro que leíste? ¿De qué trataba y qué te pareció?
:prompt_author: Talgeon

Un día, creí ser capaz de leer un libro en español. Sabía que no iba a
entenderlo perfectamente, pero es como escribir, tienes que hacerlo un día. El
primer reto fue buscar este libro. Hoy obtener un libro es algo muy fácil con
internet. Quise leer algo interesante, escrito por alguien que hablese español,
no Harry Potter.

Al final, descargué “La Fuerza de Sheccid” de Carlos Cuauhtémoc Sánchez. Pero,
fui muy sorprendido desde las primeras líneas. Creía que era un libro que
trataba sobre la vida de un joven. Es cierto que la historia trataba de un
joven, pero este joven sube en el coche de un productor pornográfico… No
entendía que era lo que estaba pasando. Me vi obligado a leer algunas veces más
las primeras páginas para entender lo que pasó. Finalmente, acabé a entenderlo,
un poco.

Las siguientes páginas se trataban del joven, su familia, su escuela, sus
amigos y Sheccid, una joven la cual chico estaba enamorado. A veces hay algunas
páginas de su diario. Me gusto un poco las palabras. Pero aun no le he
terminado. Habían frases con misoginia o homofobia, pero podía hacer como si no
las hubiera visto. Pero había cada vez más moralidad y no quiero la moralidad.
Fue bastante difícil leer en español y no quiero leer algo que me de rabia.
Ahora me doy cuenta que no he leído otro libro desde esa experiencia. ¡Vamos!
En la noche, leeré un nuevo libro.
