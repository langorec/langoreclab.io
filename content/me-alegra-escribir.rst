Me alegro de escribir
#####################

:date: 2023-01-29 10:00
:tag: alegría
:category: meta
:corrector: Fernanhaiku, cristinaaquafina101 
:langcorrect: https://langcorrect.com/journal/me-alegra-escribir/


Ha pasado un tiempo desde el comienzo pero he terminado lo que había empezado.
Con mi primer texto en español (que fue aquí), no sabía lo que iba a poner. Solo
quiero escribir. Pero, pasando un poco de tiempo aquí, me he dado cuenta que
había muchas cosas que yo podía hacer. Creía, y creo todavía, que un texto al
otro, resulta una gran historia. Es un poco extraño, las palabras no son las
mejores que podía elegir, pero es así.

Creo que mis textos y vuestras correcciones y vuestros textos y mis correcciones
(y los otros textos y las otras correcciones) son una obra que sorprende. Quise
poner todos mis textos corregidos en algún sitio. Ya es hecho. Ayer  pasé
bastante tiempo intentando de publicar estos texto en internet.

Al principio, creía que sería fácil. Después de un par de horas, me desesperé.
No entiendo como hacerlo aunque lo había hecho muchas veces hace unos años.
Después demasiadas horas, he acabado. No era perfecto pero era bastante bien. Ha
sido importante para mí porque tengo dificultades para pensar que he acabado
cualquier cosa. Ya sé que puedo publicar mí propio sitio de internet y me alegro
de esto. Y vi que soy capaz escribir en español (¡los errores no importan!). No
cabe en mí de alegría.

Me parece que he terminado algo pero solo es el principio. ¡Gracias a todos y
todas!
