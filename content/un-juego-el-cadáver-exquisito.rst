El juego del cadáver exquisito
###############################

:date: 2023-02-07 14:00
:tag: juego
:category: juegos
:corrector: Lii 
:langcorrect: https://langcorrect.com/journal/un-juego-el-cad%C3%A1ver-exquisito/

▶ Conozco un juego que se llama el “cadáver exquisito” (no estoy seguro de la
traducción, en francés es “cadavre exquis”). Les propongo que lo juguemos en
este sitio. Así son las reglas: primero alguien tiene que escribir un texto en
un primer idioma (en este caso, yo en español). La persona que me corrige (o
cualquiera persona) escribe la continuación en el idioma que quiera. La
historia va a cambiar de idioma, de sentido y no sé que va a pasar, pero podría
ser divertido… así que ¡comienzo! ◀

Entré en la cocina e inmediatamente lo vi sobre la mesa. Mi corazón latió con
mucha fuerza. Creí que no era posible, pero estaba aquí. Alguien había puesto
esa carta sobre la mesa. Él tuvo que ser muy silencio y rápido. Ya quise
leerla, pero sabía que no era posible. Me recordaba muchas cosas que habían
pasado hace mucho tiempo. Pero, estaba en la cocina, tenía que hacer cuidado.

Avancé rápidamente y con un gesto preciso, escondí la carta en mi vestido. Si
alguien me observara, pensaría que fue algo normal. Cogí el vaso sobre la mesa
y le tomé. No tenía nada de sed, pero debía actuar según el plan. En la
ventana, observé el huerto como si quisiera ver la tortuga. Pero, traté de
comprobar si alguien estaba aquí. Regresé a la última cama del pasillo y cerré
la puerta detrás de mí.

Continuará…

La continuación de Lii
----------------------

At that moment, I closed the door and I turned around. I panicked when I realized that the man was right beside me. Ha was waiting for me to enter in this room , according to his plan. He was looking at me, with those beautiful blue eyes and he took a few seconds to open his mouth and whisper "stay." Immediately I looked at his expression of sadness, just like a small dog. "Have you read the letter yet?" he said. "No... no yet" I answered him. I take it out of my pocket and I couldn't help but read the words "I still love you, please, come back."

Will continue...

La continuacíon de mari
-----------------------

ほとんど信じられなかった。また彼の手紙を読んだ。「あなたのことがまだ好きですよ。戻って来て下さい」と書いた。まず、逃げたかったが、急に、胸がいっぱいで、涙が出始まっていた。こんなにことに自分の感覚がいつも分かりにくいけど、どんなに苦しくても、本当の感覚を伝えなくてはいけないと思った。過去の痛みや間違いや倫理観を忘れて、きれいな目をゆっくり見てみて、小さい声で「あなたのことも好きです」と言った。それから、部屋が狭くなって、息を吸わないようになって、気を失う前に、「あなたのことが好きって本当だよ」とまた言った。

続きは、また改めて。
