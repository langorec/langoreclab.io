Un día
#############

:date: 2023-01-29 00:08
:tag: poco, banal
:category: meta
:corrector: brytcelf
:langcorrect: https://langcorrect.com/journal/un-día-1/

Creo que puedo escribir más en mí diario. Quiero escribir textos interesantes
pero estos necesitan mucho tiempo : tengo que buscar la idea, escribir ...

Cuando alguien hace una corrección, me encanta pero necesito poner mucha
atención. Trato de comprender las faltas y luego las correcciones. Pues escribo
una nueva versión del texto. Trato de recordar las correcciones, a veces escribo
una nota sobre estas.

Hoy he visto un par de frases bastante simples que alguien ha escrito en francés.
A primera vista, no parecía interesante pero cuando lo he corregido me
gustó porque había un montón de posibilidad detrás de todas esas palabras.

Ahora ya escribo algo así, sólo para escribir un poco.
