Ya no sé nada
#############

:slug: ya-no-sé-nada
:date: 2023-01-29 00:05
:tag: aprender, saber, creer, duda 
:category: Sujeto
:corrector: Fernanhaiku, cristinaaquafina101 
:prompt: ¿Qué es lo más importante que has aprendido en toda tu vida?
:prompt_author: Lii

No estoy seguro que es lo mas importante que he aprendido. Cuando he pensado en
eso, la primera cosa que me pareció importante es algo muy común. No sé si es
una buena forma de decirlo : un trivialidad. Es que he aprendido que no sabía
nada. He aprendido la duda, que tengo que descubrir todo y que tengo que
aprender a detenerme de dudar todo el tiempo …

Lo que quiero decir es : se podría creer que hay cosas que llamamos hechos. Por
ejemplo, si alguien me pregunta : “¿Sabes que es España?” voy a responder que
sí. ¡Seguro que sí! ¡Es demasiado fácil! Pero, si esta persona me pregunta sobre
las fronteras de eso país, voy a responder aquello : Portugal, Francia. Si estoy
listo, tal vez diré Marruecos por Ceuta y Melilla. Pero hubiera olvidado
Gibraltar y Andorra. ¡No es tan fácil como parece! No me importa saber las
fronteras de España, solo es el sujeto que imaginé. Estuve un poco en Wikipedia
y creo que es muy complejo pero no viene al caso.

Hay muchas cosas como esta que creemos saber. Creo (¿y tal vez vosotros
también?) que la Tierra es redunda. En verdad, no lo sé. Nunca lo he
comprobado yo mismo. Es más difícil con la gente: cuando encuentro alguien,
tengo que interpretar sus palabras, sus expresiones, sus tonos, sus
gestos y mucho más. Si digo “este tipo me parece bueno”, se puede que
no, solo es la impresión que he tenido cuando hablábamos.

Desde el comienzo de este texto, creía saber lo que iba a escribir. Me pregunto:
¿qué estaba escribiendo? ¿qué quería decir? ¿tiene sentido? ¿olvidé un signo de
interrogación en cualquier parte? ¿me pregunto demasiado? Entonces, creo que va
a ser el fin. Ya no sé.
