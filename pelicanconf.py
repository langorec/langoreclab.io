from pathlib import Path

AUTHOR = "bisam"
SITENAME = "Langorec"
SITEURL = ""

PATH = "content"

TIMEZONE = "Europe/Rome"

DEFAULT_LANG = "fr"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (("Pelican", "https://getpelican.com/"),)

# Social widget
SOCIAL = (("LangCorrect", "https://langcorrect.com/account/user/bisam/"),)

DEFAULT_PAGINATION = 10

THEME = "morebluepenguin"

I18N_SUBSITES = {
    "en": {"THEME_STATIC_DIR": "../theme"},
    "es": {"THEME_STATIC_DIR": "../theme"},
}
I18N_TEMPLATES_LANG = "en"

JINJA_ENVIRONMENT = {
    "extensions": ["jinja2.ext.i18n"],
}
# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

#
# blue-penguin conf
#
# provided as examples, they make ‘clean’ urls. used by MENU_INTERNAL_PAGES.
TAGS_URL = "tags"
TAGS_SAVE_AS = "tags/index.html"
CATEGORIES_URL = "categories"
CATEGORIES_SAVE_AS = "categories/index.html"
ARCHIVES_URL = "archives"
ARCHIVES_SAVE_AS = "archives/index.html"

MENU_INTERNAL_PAGES = (
    ("Tags", TAGS_URL, TAGS_SAVE_AS),
    ("Categories", CATEGORIES_URL, CATEGORIES_SAVE_AS),
    ("Archives", ARCHIVES_URL, ARCHIVES_SAVE_AS),
)
# end blue-penguin

OUTPUT_PATH = "output"
